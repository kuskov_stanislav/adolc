//----------------------------------------------------------------------
//
//   speelpenning example with automatic differentiation
//
//----------------------------------------------------------------------    
// example for fast using adol-c lib in you project
#include <stdio.h>           
#include <malloc.h>
#include "stdlib.h"
#include <math.h>
#include "adolc/adolc.h"

//----------------------------------------------------------------------
// speelpenning's function

void eval_f(double *x, int n, double *y);

//----------------------------------------------------------------------
// speelpennings's function

void eval_f_ad(double *x, int n, double *y);

//----------------------------------------------------------------------
// main program

int main (void)
{ int i,n;

    printf("n = ?  \n");
    scanf("%d",&n);

    double *x = new double[n];
    double *g = new double[n];
    double y;

    for(i=0; i<n; i++)
        x[i] = 1.0/(1.0+i);           // some initialization

    //--------------------------------------------------------------------
    // Function tracing

    eval_f_ad(x,n,&y);

//    exit(1);

    //--------------------------------------------------------------------
    // derivative computation

    gradient(1,n,x,g);

    //--------------------------------------------------------------------
    // Output

    printf(" function value: %e\n",y);
    printf("gradient:\n");
    for(i=0;i<n;i++)
        printf(" %e ",g[i]);
    printf("\n");
}

//--------------------------------------------------------------------

void eval_f(double *x, int n, double*y)
{
    int i;

    *y = 1.0;
    for(i=0;i<n;i++)
        *y = (*y)*exp(x[i]);
}

//--------------------------------------------------------------------

void eval_f_ad(double *xp, int n, double *yp)
{
    int i;
    // independents
    adouble *x   = new adouble[n];
    // dependents
    adouble y;

    trace_on(1);
    y = 1.0;
    for (i=0; i<n; i++)
    {
        x[i]<<=xp[i];
        y = y*exp(x[i]);
    }
    y >>= *yp;
    trace_off(1);
}

//----------------------------------------------------------------------
// that's it